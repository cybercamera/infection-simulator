extends Node
#Global script

var all_organisms = {} #Holds all of the organisms in the arena
var all_game_data = {} #holds all the game data to be saved

const FILE_NAME = "user://game-data.json"

var total_cells: int = 0
var total_vaccine: int = 0
var total_viruses: int = 0
var total_lifecycles: int = 0
var paused: bool = false
var syringe_active: bool = false

var organella_cell_type = preload("res://scenes/cells/Organella.tscn")
var plasmoids_cell_type = preload("res://scenes/viruses/Plasmoids.tscn")
#var pipette_cell_type = preload("res://scenes/cells/Pipette.tscn")
#var syringe_cell_type = preload("res://scenes/viruses/Syringe.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

func generate_game_data() -> void:
	all_game_data.clear()
	all_game_data["organisms"] = all_organisms
	
func load_game():
	var save_game = File.new()
	if not save_game.file_exists(FILE_NAME):
		return # Error! We don't have a save to load.
		
	# We need to revert the game state so we're not cloning objects
	# during loading. This will vary wildly depending on the needs of a
	# project, so take care with this step.
	# For our example, we will accomplish this by deleting saveable objects.
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for i in save_nodes:
		i.queue_free()
		
	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open(FILE_NAME, File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		
		# Firstly, we need to create the object and add it to the tree and set its position.
		var new_object = load(node_data["filename"]).instance()
		get_node(node_data["parent"]).add_child(new_object)
		new_object.position = Vector2(node_data["pos_x"], node_data["pos_y"])
		
		# Now we set the remaining variables.
		for i in node_data.keys():
			if i == "filename" or i == "parent" or i == "pos_x" or i == "pos_y":
				continue
			new_object.set(i, node_data[i])
			
	save_game.close()
	
# Go through everything in the persist category and ask them to return a
# dict of relevant variables.
func save_game():
	var save_game = File.new()
	save_game.open(FILE_NAME, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for node in save_nodes:
		# Check the node is an instanced scene so it can be instanced again during load.
		if node.filename.empty():
			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
			continue
			
		# Check the node has a save function.
		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue
			
		# Call the node's save function.
		var node_data = node.call("save")

		# Store the save dictionary as a new line in the save file.
		save_game.store_line(to_json(node_data))
	save_game.close()
