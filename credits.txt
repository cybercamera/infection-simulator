https://www.pngegg.com/en/png-cqjyj

Convert checkerboard background to white, keying off top left colour:
	convert png-clipart-proerythroblast-nucleated-red-blood-cell-haematopoiesis-monoblast-immune-system-purple-violet.png -fuzz 10% -fill white -draw 'color 0,0 floodfill' 11.png
Convert white background to transparent:
	convert 1.png -transparent white s1.png

Convert checkerboard background to white, keying off other parts of the image:
	convert 3.png -fuzz 10% -fill white -draw 'color 900,0 floodfill' 3b.png
	convert 3.png -fuzz 10% -fill white -draw 'color 800,0 floodfill' 3b.png
  	convert 4b.png -fuzz 10% -fill white -draw 'color 899,940 floodfill' 4c.png

https://github.com/williambcosta/godot-touch-camera-2d
https://wallpapertag.com/hex-grid-wallpaper
https://www.theleagueofmoveabletype.com/orbitron
https://www.youtube.com/watch?v=lQbQWW1NBFU <-  Passing Data With Signals in Godot
https://www.youtube.com/watch?v=hvAcWPLI3NQ <- custom animated cursor
https://github.com/godotengine/godot/issues/31229 <- save nodes to disk, then load them as new instances
https://github.com/ttwings/WuXiaAndJiangHu_Godot/commit/88e297b2bda247e196ad6c589f6b5371f0198a2d <- save objects to disk, parse then and instance new version on file load
https://docs.godotengine.org/en/stable/tutorials/io/saving_games.html
https://www.youtube.com/watch?v=bHfcWYoVVfg <- droplist
http://pngimg.com/image/13487
https://cdn2.iconfinder.com/data/icons/medical-equipment-supplies-1/256/Chemical_Dropper-512.png
https://www.flaticon.com/free-icon/pipette_308726
https://www.flaticon.com/free-icon/pipette_4445029
https://kidscancode.org/godot_recipes/ui/radial_menu/


Cell names
----------

Organella
Ribosomes
Membranelles
Cytoploids
Nucleons
Endoplasms
Mitochondrates
Plastids

Virus names
-----------
Cilialla
Centrioles
Vacuella
Plasmoids

