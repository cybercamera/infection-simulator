extends KinematicBody2D

signal cell_death
signal cell_is_vaccinatable

export var max_speed : int = 20 #default max speed
export var speed_randomness : float = 1.0 #How much max_speed vthe speedaries. The higher the value (close to one) the less variable 
export var can_connect_with_other_cells: bool = false
export var proximity_scaling: float = 1.0
export var immunity_level: int = 30
export var vaccination_level: int = 10
export var stop_processing: bool = false

var animation_playback_speed : float = 1.0

const MAX_PROXIMITY_SCALING = 5.0
const PROCESS_PRIORITY = 10
const ANTIBODY_GENERATION_AMOUNT = 10 #this much per cycle

var viral_vectors = ["Plasmoids"]  #What viruses can infect this cell
var viral_infectors = [] #Which viruses *have* infected this cell
var viral_infection_level : int = 0
var antibody_level : int = 0

var speed : int = 0
var velocity = Vector2()
var connected_with_other_cell : bool = false 
var life_cycles: int = 0
var can_display_infobox : bool = false
var current_animation : String = "ThrobbingCell"

func _ready():
	add_to_group("cells")
	add_to_group("organella")
	add_to_group("Persist") #This is used for finding entities to save in the save file.
	process_priority = PROCESS_PRIORITY
	proximity_scaling = clamp(proximity_scaling, 0.1, MAX_PROXIMITY_SCALING)
	$ProximityArea.scale = Vector2(proximity_scaling, proximity_scaling)
	$AnimationPlayer.play("ThrobbingCell")

func _physics_process(delta):
	if !stop_processing:
		#print(str(self.get_name()))
		move_and_collide(velocity * speed * delta)
	
func generate_cell_infoblock() -> String:
	var infoblock_text: String
	infoblock_text += "\n[indent][color=blue]Name: " + "[/color] [color=green]" + str(self.get_name()) + "[/color][/indent]\n"
	infoblock_text += "[indent][color=blue]Life Cycles: " + "[/color] [color=green]" + str(life_cycles) + "[/color][/indent]\n"
	infoblock_text += "[indent][color=blue]Immunity Level: " + "[/color] [color=purple]" + str(immunity_level) + "[/color][/indent]\n" 
	#We will likely need a different immunity level against each different virus
	
	return infoblock_text

func cell_death() -> void:
	var unique_viruses = []
	#Start the process of cell death.
	#Let's start by no longer interacting with anything
	$CollisionPolygon2D.disabled = true #Switch off our collision mechanisms
	$ContactArea/CollisionPolygon2D.disabled = true
	#We iterate through our list of infectors and spawn some random number of each of those virsues
	for virus in viral_infectors:
		#Iterate through array
		if !unique_viruses.has(virus): #Check to see if we've seen this already
			unique_viruses.append(virus)
			var virus_scene = load("res://scenes/viruses/" + virus + ".tscn")
			if get_parent().is_a_parent_of(self):
				#We need to create a random numner of instances of a that virus
				for i in range(3):
					var virus_instance = virus_scene.instance()
					get_parent().add_child(virus_instance) #Add to level node
					virus_instance.scale = Vector2(0.01, 0.01) # make the virus small to spawn. Grow lat
					virus_instance.position = self.global_position   + Vector2(rand_range(-50,50), rand_range(-50,50))
					#grow the spawned virus
					$VirusSpawnTween.interpolate_property(virus_instance, "scale", virus_instance.scale, Vector2(0.2,0.2), 2.0, Tween.TRANS_QUART, Tween.EASE_OUT)
					$VirusSpawnTween.start()
			
	$Droplets.visible = true
	$Droplets.emitting = true
	$FadeTween.interpolate_property($Sprite, "modulate", $Sprite.modulate, Color($Sprite.modulate.a, $Sprite.modulate.a, $Sprite.modulate.a, 0), 1.0, Tween.TRANS_QUART, Tween.EASE_OUT)
	print("Start tween")
	$FadeTween.start()
	emit_signal("cell_dath")
 

func connecting_to_virus(body):
	#we've been told what we've connected to a virus. 
	#Have to now listen to virus for viral load inection
	print("connection to virus")
	if body:
		print("connecting to: ", body)
		body.connect("injecting_virus", self, "receive_viral_load")

func connecting_to_syringe(body):
	#we've been told what we've connected to a virus. 
	#Have to now listen to virus for viral load inection
	#print("connection to syringe")
	if body:
		print("connecting to: ", body.get_name())
		body.connect("injecting_vaccine", self, "receive_vaccination_load")

func disconnecting_from_syringe(body):
	#we've been told what we've connected to a virus. 
	#Have to now listen to virus for viral load inection
	#print("connection to syringe")
	if body:
		print("disconnecting from: ", body.get_name())
		body.disconnect("injecting_vaccine", self, "receive_vaccination_load")

func receive_vaccination_load(virus_type, vaccination_load):
	vaccination_level += vaccination_load
	print("Vaccinated: ",str(vaccination_level)) 
	animation_playback_speed += 0.2
	print("animation_playback_speed = ", str(clamp(animation_playback_speed, 1, 3)))
	$AnimationPlayer.playback_speed = clamp(animation_playback_speed, 1, 3)
#	vaccination_infectors.append(virus_type)
#	if viral_infection_level > immunity_level:
#		cell_death()
#	else:
#		produce_antibodies()

func receive_viral_load(virus_type, viral_load):
	#First we check to see if this virus is  a threat to us. If yes, then infect
	#print("receive_viral_load: " , virus_type,  str(viral_load))
	if viral_vectors.count(virus_type) > 0:
		#print("this virus infects us")
		$AnimationPlayer.play("Infected")
		current_animation = "Infected"
		viral_infection_level += viral_load
		$AnimationPlayer.playback_speed += 1.0
		viral_infectors.append(virus_type)
		if viral_infection_level > immunity_level:
			cell_death()
		else:
			produce_antibodies()
			
func produce_antibodies() -> void:
	#produce antibodies
	$AntibodyTimer.start()

func save():
	var save_dict = {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"max_speed" : max_speed,
		"speed_randomness" : speed_randomness,
		"can_connect_with_other_cells" : can_connect_with_other_cells,
		"proximity_scaling" : proximity_scaling,
		"immunity_level" : immunity_level,
		"vaccination_level" : vaccination_level,
		"speed" : speed,
		"connected_with_other_cell" :connected_with_other_cell,
		"life_cycles" : life_cycles,
		"stop_processing" : stop_processing,
		"viral_infection_level" : viral_infection_level,
		"antibody_level" : antibody_level,
		"viral_infectors" : viral_infectors,
		"animation_playback_speed" : animation_playback_speed 
	}
	return save_dict

func _on_MovementTimer_timeout():
	life_cycles += 1
	#Hide the infobox, in case it's been left on
	$Infobox.hide()
	
	if !can_display_infobox:
		can_display_infobox = true
		
	#print("stop_processing:", str(stop_processing))	
	if !stop_processing:
		if connected_with_other_cell:
			#We're connected. Don't move any more
			stop_processing = true
			self.pause_mode = PAUSE_MODE_STOP
			velocity = Vector2.ZERO
			#$MovementTimer.stop()
		else:
			var rand_speed = clamp(speed_randomness, 0, 1)
			speed =  max_speed - max_speed * rand_range(0,rand_speed)
			#print(str(speed))
			velocity = Vector2(rand_range(-1,1), rand_range(-1,1)).normalized()
		
		if position.x < -10000 or position.x > 100000 or position.y < -100000 or position.y > 100000:
				#Entity has left the screen, so kill it
				queue_free()

func _on_ContactArea_body_entered(body):
	var other_cell_id = body.get_instance_id()
	#print("body: " + str(body.get_instance_id()) + " self: " + str(other_cell_id))
	print(str(self.get_name()) +  " touching an organism: " + str(body))
	if other_cell_id != self.get_instance_id():
		#print("touching: " + str(body))
		connected_with_other_cell = true
		if body.get_groups().has("viruses"):
			#We are here becuase we've connected to a virus
			connecting_to_virus(body)
		
	
func _on_ProximityArea_area_entered(area):
	if area.get_parent().get_instance_id() != self.get_instance_id():
		#We are here because we're not sensing ourselves
		#print("area_id: " + str(area.get_parent().get_instance_id()) + "self_id: " + str(self.get_instance_id()))
		if !connected_with_other_cell:
			#We can only move if we're not connected with another cell yet
			var area_body = area.get_parent()
			if (area_body.get_groups().has("cells") and can_connect_with_other_cells) or area_body.get_groups().has("organella"):
				velocity = self.position.direction_to(area_body.position)
				#print("self.position: " + str(self.position) + " area_body.position: " + str(area_body.position))
				velocity = velocity.normalized()
				#print("proximity_area_entered: velocity = " + str(velocity))
				#$MovementTimer.stop()
		
			
func _on_ContactArea_area_entered(area):
	var area_body = area.get_parent()
	if area_body.get_instance_id() != self.get_instance_id() and area.get_name() == "ContactArea":
		#print("contact_area_entered")
		
		if (area_body.get_groups().has("cells") and can_connect_with_other_cells) or (area_body.get_groups().has("organella")):
			connected_with_other_cell = true
			
		if area_body.get_groups().has("viruses"):
			#We are here becuase we've connected to a virus
			connecting_to_virus(area.get_parent())
		
	if area_body.get_groups().has("syringe_cursor"):
		connecting_to_syringe(area_body)
		
func _on_ContactArea_input_event(viewport, event, shape_idx):
	if Utils.syringe_active:
		emit_signal("cell_is_vaccinatable")


func _on_ContactArea_area_exited(area):
	#Check to see if it's the syringe leaving us. If so, break the connection
	var area_body = area.get_parent()
	if area_body.get_groups().has("syringe_cursor"):
			disconnecting_from_syringe(area_body)
	
func _on_ContactArea_mouse_entered():
	#print("mouse_entered:", self.get_name())
	#print("can_display_infobox", str(can_display_infobox))
	if can_display_infobox:
		$Infobox/Infoblock.bbcode_text = generate_cell_infoblock()
		$Infobox.show()
	

func _on_ContactArea_mouse_exited():
	$Infobox.hide()


func _on_AntibodyTimer_timeout():
	#We keep adding this much per cycle
	#The higher the viral load, the greater the level of antibody generation
	#It will be up to the player to extract these antibodies before the cell dies
	antibody_level += ANTIBODY_GENERATION_AMOUNT + viral_infection_level
	print("antibody_level: " + str(antibody_level))


func _on_FadeTween_tween_completed(object, key):
	queue_free()


func _on_VisibilityNotifier2D_screen_entered():
	$AnimationPlayer.play(current_animation)

func _on_VisibilityNotifier2D_screen_exited():
	$AnimationPlayer.stop()



