extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	#$CellStatusScanTimer.start()
	pass

func count_all_cells(node) -> void:
	#iterate through all of the cells
	for n in node.get_children():
		if n.get_groups().has("cells"):
			#print("["+n.get_name()+"] " + str(n.pause_mode))
			Utils.total_cells += 1
		elif n.get_child_count() > 0:
			#not a cell, but may host cell children. Delve deeper.
			#print("["+n.get_name()+"]")
			count_all_cells(n)
			
func count_all_viruses(node) -> void:
	#iterate through all of the viruses
	for n in node.get_children():
		if n.get_groups().has("viruses"):
			#print("["+n.get_name()+"] " + str(n.pause_mode))
			Utils.total_viruses += 1
		elif n.get_child_count() > 0:
			#not a cell, but may host cell children. Delve deeper.
			#print("["+n.get_name()+"]")
			count_all_viruses(n)

func reap_old_viruses(node) -> void:
	#We call this function once performance is too lows
	for n in node.get_children():
		if n.get_groups().has("viruses"):
			#print("reaping check: " + str(n))
			#print("["+n.get_name()+"] " + str(n.pause_mode))
			var virus_life_cycles = n.get_life_cycles_count()
			if virus_life_cycles > 3: #if older than 3 cycles, reap
				print("reaping: " + str(n))
				n.virus_die()
		

func get_all_organisms(node) -> void:
	#iterate through all of the viruses
	for n in node.get_children():
		if n.get_groups().has("viruses") or n.get_groups().has("cells"):
			#print("["+n.get_name()+"] " + str(n.pause_mode))
			Utils.all_organisms[n.get_name()] = var2str(n)
		elif n.get_child_count() > 0:
			#not a cell, but may host cell children. Delve deeper.
			#print("["+n.get_name()+"]")
			get_all_organisms(n)


func _on_CellStatusScanTimer_timeout():
	Utils.total_cells = 0 #reset the cell count
	Utils.total_viruses = 0 #reset the cell count
	count_all_cells(self)
	count_all_viruses(self)
	#get_all_organisms(self) 
	if Performance.get_monitor(Performance.TIME_FPS) < 30:
		reap_old_viruses(self)
