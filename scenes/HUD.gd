extends CanvasLayer

signal hide_current_cell_cursor
signal show_current_cell_cursor

var mouse_cell_cursor = preload("res://scenes/mouse_cursors/MouseCursorCells.tscn")
var mouse_pipette_cursor = preload("res://scenes/mouse_cursors/MouseCursorPipette.tscn")

var current_cell_cursor

#Radial button variables
export var radius = 120
export var speed = 0.25

var num_vaccination_childred : int 
var vaccination_menu_active = false
var vaccination_menu_spawnpoint =  Vector2(0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	$Controls/CellPanel/ButtonSyringe/Buttons.hide()
	var HUD = get_parent().get_node("HUD")
	#print("HUD node: " + str(HUD))
	#Cobnnect to the 
	$Controls/CellPanel/ButtonSyringe/Buttons/ButtonCentrioles.connect("hide_vaccination_menu_signal", self, "hide_vaccination_menu")
	$Controls/CellPanel/ButtonSyringe/Buttons/ButtonCilialla.connect("hide_vaccination_menu_signal", self, "hide_vaccination_menu")
	$Controls/CellPanel/ButtonSyringe/Buttons/ButtonVacuella.connect("hide_vaccination_menu_signal", self, "hide_vaccination_menu")
	$Controls/CellPanel/ButtonSyringe/Buttons/ButtonPlasmoids.connect("hide_vaccination_menu_signal", self, "hide_vaccination_menu")
	num_vaccination_childred = $Controls/CellPanel/ButtonSyringe/Buttons.get_child_count()
	for b in $Controls/CellPanel/ButtonSyringe/Buttons.get_children():
		#b.rect_position = $Controls/CellPanel/ButtonSyringe/Buttons.rect_position
		b.rect_position = vaccination_menu_spawnpoint
#		print("_ready b.rect_position: ", str(b.rect_position))
#		print("_ready b.rect_size: " + str(b.rect_size))
	


func _process(delta):
	get_input()
		
func get_input() -> void:
	if Input.is_action_pressed("ui_cancel"):
		hide_vaccination_menu()


func _on_ButtonQuit_pressed():
	get_tree().quit()
	
func new_game() -> void:
	Utils.total_cells = 0
	Utils.total_vaccine = 0
	Utils.total_viruses = 0
	Utils.total_lifecycles = 0
	get_tree().change_scene("res://scenes/Arena.tscn")

func hide_message():
	$Information.visible = false
	$Information.text = ""

func show_vaccination_menu():
	print("showing")
	$Controls/CellPanel/ButtonSyringe/Buttons.show()
	var spacing = TAU / num_vaccination_childred
	for b in $Controls/CellPanel/ButtonSyringe/Buttons.get_children():
		var a = spacing * b.get_position_in_parent() - PI / 2
		var dest = b.rect_position + Vector2(radius, 0).rotated(a)
#		print("radius: " + str(radius))
		$Controls/CellPanel/ButtonSyringe/VaccinationTween.interpolate_property(b, "rect_position", b.rect_position,
				dest, speed, Tween.TRANS_BACK, Tween.EASE_OUT)
		$Controls/CellPanel/ButtonSyringe/VaccinationTween.interpolate_property(b, "rect_scale", Vector2(0.5, 0.5),
				Vector2.ONE, speed, Tween.TRANS_LINEAR)
#		print("show_menu dest", str(dest))
#		print("show_menu rect_size: " + str(b.rect_size))
		#print("buttons node margin left: ", str($Controls/CellPanel/ButtonSyringe/Buttons.margin_left))
		#print("buttons node margin right: ", str($Controls/CellPanel/ButtonSyringe/Buttons.margin_right))
	$Controls/CellPanel/ButtonSyringe/VaccinationTween.start()


func hide_vaccination_menu():
	print("hiding")
	for b in $Controls/CellPanel/ButtonSyringe/Buttons.get_children():
		$Controls/CellPanel/ButtonSyringe/VaccinationTween.interpolate_property(b, "rect_position", b.rect_position,
				vaccination_menu_spawnpoint, speed, Tween.TRANS_BACK, Tween.EASE_IN)
		$Controls/CellPanel/ButtonSyringe/VaccinationTween.interpolate_property(b, "rect_scale", null,
				Vector2(0.5, 0.5), speed, Tween.TRANS_LINEAR)
		#print("hide_menu", str($Controls/CellPanel/ButtonSyringe.rect_position))
#		print("hide_menu dest", str(vaccination_menu_spawnpoint))
#		print("hide_menu rect_size: " + str(b.rect_size))
	$Controls/CellPanel/ButtonSyringe/VaccinationTween.start()
	

func mouse_entered() -> void:
	if  current_cell_cursor and $MouseExitTimer.is_stopped():
		#print("emit signal")
		emit_signal("hide_current_cell_cursor")

func mouse_exited() -> void:
	if  current_cell_cursor:
		#print("emit signal")
		emit_signal("show_current_cell_cursor")

func show_message(message_text: String):
	$Information.text = message_text
	$Information.visible = true

func show_temp_message(message_text: String) -> void:
	show_message(message_text)
	yield(get_tree().create_timer(3.0), "timeout")
	hide_message()

func _on_Controls_mouse_entered():
	mouse_entered()


func _on_Controls_mouse_exited():
	mouse_exited()
	

func _on_GameTimer_timeout():
	Utils.total_lifecycles += 1
	$Infobox/Cells.text = "Cells: " + str(Utils.total_cells)
	$Infobox/Vaccine.text = "Vaccine: " + str(Utils.total_vaccine)
	$Infobox/Viruses.text = "Viruses: " + str(Utils.total_viruses)
	$Infobox/Lifecycles.text = "Lifecycles: " + str(Utils.total_lifecycles)


func _on_ButtonNew_pressed():
	new_game()


func _on_ButtonSave_pressed():
	Utils.save_game()
	show_temp_message("Game Saved.")


func _on_ButtonLoad_pressed():
	Utils.load_game()
	show_temp_message("Game Loaded.")


func _on_ButtonPause_pressed():
	if Utils.paused: 
		#we're already paused, let's unpause
		hide_message()
		Utils.paused = not Utils.paused
		get_tree().set_pause(Utils.paused)
		
	else:
		#let's pause
		Utils.paused = not Utils.paused
		get_tree().set_pause(Utils.paused)
		show_message("Simulator Paused!")


func _on_CellPanel_mouse_entered():
	mouse_entered()

func _on_Infobox_mouse_entered():
	mouse_entered()

func _on_ButtonOptions_pressed():
	print("$Options.stop_processing: " + str($Options.stop_processing))
	if $Options.stop_processing:
		$Options.show()
		$Options.stop_processing = false
	else:
		$Options.hide()
		$Options.stop_processing = true

func _on_ButtonOrganella_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Organella"
		current_cell_cursor = new_cursor
		$MouseExitTimer.start()

func _on_ButtonOrganella_mouse_entered():
	mouse_entered()

func _on_ButtonOrganella_mouse_exited():
	mouse_exited()


func _on_ButtonRibosomes_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()

func _on_ButtonRibosomes_mouse_entered():
	mouse_entered()
	
func _on_ButtonRibosomes_mouse_exited():
	mouse_exited()

func _on_ButtonMembranelles_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()
	


func _on_ButtonMembranelles_mouse_entered():
	mouse_entered()


func _on_ButtonMembranelles_mouse_exited():
	mouse_exited()


func _on_ButtonCytoploids_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()


func _on_ButtonCytoploids_mouse_entered():
	mouse_entered()


func _on_ButtonCytoploids_mouse_exited():
	mouse_exited()


func _on_ButtonNucleons_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()


func _on_ButtonNucleons_mouse_entered():
	mouse_entered()


func _on_ButtonNucleons_mouse_exited():
	mouse_exited()


func _on_ButtonEndoplasms_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()


func _on_ButtonEndoplasms_mouse_entered():
	mouse_entered()


func _on_ButtonEndoplasms_mouse_exited():
	mouse_exited()


func _on_ButtonMitochondrates_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()


func _on_ButtonMitochondrates_mouse_entered():
	mouse_entered()


func _on_ButtonMitochondrates_mouse_exited():
	mouse_exited()


func _on_ButtonPlastids_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_cell_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Ribosomes"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()


func _on_ButtonPlastids_mouse_entered():
	mouse_entered()


func _on_ButtonPlastids_mouse_exited():
	mouse_exited()


func _on_ButtonPipette_pressed():
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_pipette_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.cell_type_cursor = "Pipette"
		get_parent().add_child(new_cursor) #Add to level node
		new_cursor.init()
		current_cell_cursor = new_cursor
		print("$MouseExitTimer.start()")
		$MouseExitTimer.start()


func _on_ButtonPipette_mouse_entered():
	mouse_entered()


func _on_ButtonPipette_mouse_exited():
	mouse_exited()

func _on_ButtonSyringe_pressed():
	#print("_on_ButtonSyringe_pressed(): vaccination_menu_active ", str(vaccination_menu_active))
	$MouseExitTimer.start()
	current_cell_cursor = true
	#$Controls/CellPanel/ButtonSyringe.disabled = true
	if vaccination_menu_active:
		hide_vaccination_menu()
	else:
		show_vaccination_menu()
		#$Controls/CellPanel/ButtonSyringe.disabled = true

func _on_ButtonSyringe_mouse_entered():
	mouse_entered()


func _on_ButtonSyringe_mouse_exited():
	mouse_exited()

func _on_VaccinationTween_tween_all_completed():
	#print("_on_VaccinationTween_tween_all_completed")
	#$Controls/CellPanel/ButtonSyringe.disabled = false
	vaccination_menu_active = not vaccination_menu_active
	$Controls/CellPanel/ButtonSyringe.disabled = false
	if not vaccination_menu_active:
		#print("$Controls/CellPanel/ButtonSyringe/Buttons.hide()")
		$Controls/CellPanel/ButtonSyringe/Buttons.hide()
		
