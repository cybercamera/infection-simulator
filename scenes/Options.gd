extends Container

export var stop_processing: bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	add_option_items()

func _process(delta):
	if !stop_processing:
		print(".")
		if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_cancel"):
			hide_options()
			
func add_option_items() -> void:
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: 1024 x 630")
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: 1440 x 900")
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: 1920 x 1024")
	$ScrollContainer/VerticalList/DisplaySize.add_item("Display Size: Full Size")
	
func hide_options():
	self.hide()
	stop_processing = true


func _on_Close_pressed():
	hide_options()


func _on_DisplaySize_item_selected(index):
	#Set the screen size based on option selected by user
	match index:
		0:
			OS.set_window_fullscreen(false)
			OS.set_window_size(Vector2(1024, 630))
		1:
			OS.set_window_fullscreen(false)
			OS.set_window_size(Vector2(1440, 900))
		2:
			OS.set_window_fullscreen(false)
			OS.set_window_size(Vector2(1920, 1024))
		3:
			OS.set_window_fullscreen(true)
		_:
			# If nothing matches, run this block of code
			pass
