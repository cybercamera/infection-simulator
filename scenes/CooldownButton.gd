extends TextureButton

signal hide_vaccination_menu_signal

export(String, "Centrioles", "Plasmoids", "Cilialla", "Vacuella") var virus_type_cursor  = "Centrioles"

export var cooldown = 1.0

var mouse_syringe_cursor = preload("res://scenes/mouse_cursors/MouseCursorSyringe.tscn")


func _ready():
	print("rect_size: ", str(rect_size))
	$Label.hide()
	$CooldownDisplay.value = 0
	$CooldownDisplay.texture_progress = texture_normal
	$Timer.wait_time = cooldown
	#set_process(false)


func _process(delta):
	$Label.text = "%3.1f" % $Timer.time_left
	$CooldownDisplay.value = int(($Timer.time_left / cooldown) * 100)
	
	
func _on_CooldownButton_pressed():
	print("_on_CooldownButton_pressed(): " + str(self.get_instance_id()))
	set_process(true)
	#disabled = true
	$Timer.start()
	$Label.show()


func _on_Timer_timeout():
	#print("ability ready")
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a mouse cursor and attach it to the parent level 
		var new_cursor = mouse_syringe_cursor.instance()
		new_cursor.position = get_viewport().get_mouse_position()
		new_cursor.vaccination_type = virus_type_cursor
		get_tree().get_root().get_node("Arena").add_child(new_cursor) #Add to arena - is this right?
		new_cursor.init()
		emit_signal("hide_vaccination_menu_signal")
		#we don't need any mouse cursor exit code, as we're not in the toolbar
		#current_cell_cursor = new_cursor
		
	$CooldownDisplay.value = 0
	disabled = false
	$Label.hide()
	set_process(false)
	
