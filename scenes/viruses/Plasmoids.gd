extends KinematicBody2D

signal injecting_virus

export var max_speed : int = 100 #default max speed
export var speed_randomness : float = 1.0 #How much max_speed vthe speedaries. The higher the value (close to one) the less variable 
export var proximity_scaling: float = 1.0
export var viral_load: int = 10
export var tracking_distance: int = 1000 #How far to cast the Raycast2d
export var stop_processing: bool = false

const MAX_PROXIMITY_SCALING = 5.0
const PROCESS_PRIORITY = 9
const VIRUS_TYPE = "Plasmoids"

var speed : int = 0
var velocity = Vector2()
var connected_with_other_cell : bool = false 
var life_cycles: int = 0
var can_display_infobox : bool = false
var target_located: bool = false
var target_body
var tracking_duration_counter: int = 0 #If we track something more than 30 seconds and still not made contact, rescan
var virus_dead : bool = false

func _ready():
	add_to_group("viruses")
	add_to_group(VIRUS_TYPE)
	add_to_group("Persist") #This is used for finding entities to save in the save file.
	process_priority = PROCESS_PRIORITY
	proximity_scaling = clamp(proximity_scaling, 0.1, MAX_PROXIMITY_SCALING)
	$ProximityArea.scale = Vector2(proximity_scaling, proximity_scaling)
	velocity = Vector2.ZERO
	$Sprite/Scanner.cast_to = Vector2(tracking_distance, tracking_distance)
	$Sprite/Scanner.enabled = true
	

func _physics_process(delta):
	if !stop_processing:
		#print(str(self.get_name()))
		move_and_collide(velocity * speed * delta)
		$Sprite.rotation -= 1 * delta
		if !target_located: 
			find_target_cell()

func find_target_cell():
	#We have to rotate the scanner until we find a possible target cell
	#then we make a normalised velocity vector to that target cell
	#then we return that vecgtor
	if $Sprite/Scanner.is_colliding():
			#print("Scanner collision")
			var target = $Sprite/Scanner.get_collider()
			#print("Target: ", str(target.get_name()))
			if target.is_in_group("cells"):
				velocity =  (self.position.direction_to(target.position)).normalized()
				#We've found the target cell. Let us be told when it dioes
				target.connect("cell_death", self, "target_gone")
				target_located = true
	elif velocity.length() < 0.05:
		#If our velocity is very low, then set up a new random velocity
		velocity = Vector2(rand_range(-1,1), rand_range(-1,1)).normalized()
		
	
	#print("target velocity: ", str(velocity))
	
func generate_cell_infoblock() -> String:
	var infoblock_text: String
	infoblock_text += "\n[indent][color=blue]Name: " + "[/color] [color=green]" + str(self.get_name()) + "[/color][/indent]\n"
	infoblock_text += "[indent][color=blue]Life Cycles: " + "[/color] [color=green]" + str(life_cycles) + "[/color][/indent]\n"
	infoblock_text += "[indent][color=blue]Viral Load: " + "[/color] [color=purple]" + str(viral_load) + "[/color][/indent]\n" 
	#We will likely need a different immunity level against each different virus
	
	return infoblock_text

func get_life_cycles_count() -> int:
	return life_cycles

func inject_virus():
	#set-up emit signal to connecting body
	#print("Emitting inject_virus signal")
	emit_signal("injecting_virus", VIRUS_TYPE, viral_load)
	virus_die()
	
func target_gone():
	#we are heere because our target is gone. 
	target_located = false
	
func virus_die():
	$Droplets.visible = true
	$Droplets.emitting = true
	$FadeTween.interpolate_property($Sprite, "modulate", $Sprite.modulate, Color($Sprite.modulate.a, $Sprite.modulate.a, $Sprite.modulate.a, 0), 0.5, Tween.TRANS_QUART, Tween.EASE_OUT)
	$FadeTween.start()
	
	
	
func _on_MovementTimer_timeout():
	life_cycles += 1
	tracking_duration_counter += 1
	#Hide the infobox, in case it's been left on
	$Infobox.hide()
	
	if !can_display_infobox:
		can_display_infobox = true
		
	#print("stop_processing:", str(stop_processing))	
	if !stop_processing:
		if connected_with_other_cell:
			#We're connected. Don't move any more
			$Sprite/Scanner.enabled = false
			stop_processing = true
			self.pause_mode = PAUSE_MODE_STOP
			velocity = Vector2.ZERO
			$MovementTimer.stop()
			inject_virus()
		else:
			var rand_speed = clamp(speed_randomness, 0, 1)
			speed =  max_speed - max_speed * rand_range(0,rand_speed)
		
		if position.x < -10000 or position.x > 100000 or position.y < -100000 or position.y > 100000:
				#Entity has left the screen, so kill it
				queue_free()
	
	if tracking_duration_counter > 3:
		#If we track something more than 30 seconds and still not made contact, rescan
		target_located = false
		tracking_duration_counter
		stop_processing = false
		$AnimationPlayer.playback_speed = 1
		
func save():
	var save_dict = {
		"filename" : get_filename(),
		"parent" : get_parent().get_path(),
		"pos_x" : position.x, # Vector2 is not supported by JSON
		"pos_y" : position.y,
		"max_speed" : max_speed,
		"speed_randomness" : speed_randomness,
		"proximity_scaling" : proximity_scaling,
		"viral_load" : viral_load,
		"tracking_distance" : tracking_distance,
		"speed" : speed,
		"connected_with_other_cell" :connected_with_other_cell,
		"life_cycles" : life_cycles,
		"stop_processing" : stop_processing
	}
	return save_dict

func _on_ContactArea_body_entered(body):
	var other_cell_id = body.get_instance_id()
	#print("body: " + str(body.get_instance_id()) + " self: " + str(other_cell_id))
	
	if other_cell_id != self.get_instance_id():
		if 	body.get_groups().has("cells"):
			#print(str(self.get_name()) +  " touching a cell: " + str(body))
			connected_with_other_cell = true
			target_body = body
			$AnimationPlayer.playback_speed = 3
		elif body.get_groups().has("viruses"):
			#it's a virus. Go in other direction
			velocity =  Vector2(-self.position.direction_to(body.position).normalized().x, -self.position.direction_to(body.position).normalized().y)
		

func _on_ContactArea_area_entered(area):
	if area.get_parent().get_instance_id() != self.get_instance_id() and area.get_name() == "ContactArea":
		#print("contact_area_entered: " + str(area))
		if area.get_parent().get_groups().has("cells"):
			print("contact_area_entered: " + str(area.get_parent().get_name()))
			connected_with_other_cell = true
			target_body = area.get_parent()
			$AnimationPlayer.playback_speed = 3
		elif area.get_parent().get_groups().has("viruses"):
			#it's a virus. Go in other direction
			#print("contact_area_entered: " + str(area.get_parent().get_name()))
			velocity =  Vector2(-self.position.direction_to(area.get_parent().position).normalized().x, -self.position.direction_to(area.get_parent().position).normalized().y)
			#print(str(velocity))

func _on_ContactArea_mouse_entered():
	#print("mouse_entered:", self.get_name())
	#print("can_display_infobox", str(can_display_infobox))
	if can_display_infobox:
		$Infobox/Infoblock.bbcode_text = generate_cell_infoblock()
		$Infobox.show()
	

func _on_ContactArea_mouse_exited():
	$Infobox.hide()


func _on_ProximityArea_area_entered(area):
	if area.get_parent().get_instance_id() != self.get_instance_id():
		#We are here because we're not sensing ourselves
		#print("area_id: " + str(area.get_parent().get_instance_id()) + " self_id: " + str(self.get_instance_id()))
		if !connected_with_other_cell:
			#We can only move if we're not connected with another cell yet
			var area_body = area.get_parent()
			if area_body.get_groups().has("cells"):
				velocity = self.position.direction_to(area_body.position)
				#print("self.position: " + str(self.position) + " area_body.position: " + str(area_body.position))
				velocity = velocity.normalized()
				#print("proximity_area_entered: velocity = " + str(velocity))



func _on_FadeTween_tween_completed(object, key):
	if virus_dead:
		#We are here because both virus and droplets are done, so queue free
		queue_free()
	else:
		#We are here because both virus is gone, but droplets remain to be shown
		$FadeTween.interpolate_property($Droplets, "modulate", $Droplets.modulate, Color($Droplets.modulate.a, $Droplets.modulate.a, $Droplets.modulate.a, 0), 0.5, Tween.TRANS_QUART, Tween.EASE_OUT)
		$FadeTween.start()
		virus_dead = true
		
	
func _on_VisibilityNotifier2D_screen_entered():
	#print("_on_VisibilityNotifier2D_screen_entered()")
	$AnimationPlayer.play("ThrobbingCell")
	#stop_processing = false
	
func _on_VisibilityNotifier2D_screen_exited():
	#print("_on_VisibilityNotifier2D_screen_exited()")
	$AnimationPlayer.stop()
	#stop_processing = true
