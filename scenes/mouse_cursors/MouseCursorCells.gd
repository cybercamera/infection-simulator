extends Node2D

export(String, "Organella", "Plasmoids", "Pipette", "Syringe") var cell_type_cursor  = "Organella"

#var HUD
#var HUD_controls
#var HUD_controls_organella

var click_timer_active: bool = false
var cell_cursor_active: bool = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	#Hook up to the HUD to get signals of cursor moving over panel
	var HUD = get_parent().get_node("HUD")
#	HUD_controls = get_parent().get_node("HUD").get_node("Controls")
#	HUD_controls_organella = get_parent().get_node("HUD").get_node("Controls")
	if HUD:
			HUD.connect("hide_current_cell_cursor", self, "hide_cell_cursor")
			HUD.connect("show_current_cell_cursor", self, "show_cell_cursor")
#		HUD_controls.connect("hide_current_cell_cursor", self, "hide_cell_cursor")
#		HUD_controls.connect("show_current_cell_cursor", self, "show_cell_cursor")
#		HUD_controls_organella.connect("hide_current_cell_cursor", self, "hide_cell_cursor")
#		HUD_controls_organella.connect("show_current_cell_cursor", self, "show_cell_cursor")

func _process(delta):
	self.position = self.get_global_mouse_position()
	get_input()
	
func clear_cell_cursor() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	queue_free()
		
func get_input():
	if !click_timer_active and (Input.is_action_pressed("ui_mouse_left_click") or Input.is_action_pressed("ui_accept")):
		print("active mouse click")
		click_timer_active = true
		$ClickTimer.start()
		spawn_cell()
	if Input.is_action_pressed("ui_cancel"):
		clear_cell_cursor()
		

func hide_cell_cursor() -> void:
	#We should only be able to clear the cell cursor when the cursor has moved over the panel for the first time
	#But only after it has left the panel for the first time
	print("hide_cell_cursor()")
	if cell_cursor_active:
		clear_cell_cursor()
		cell_cursor_active = false

func show_cell_cursor() -> void:
	#We should only be able to clear the cell cursor when the cursor has moved over the panel for the first time
	print("show_cell_cursor()")
	cell_cursor_active = true
	
	
func init():
	#This is called after the scene is instanced 
	if get_parent().is_a_parent_of(self):
		var cursor_filename = "res://assets/sprites/" + cell_type_cursor + ".png"
		print(cursor_filename)
		$Sprite.texture = load(cursor_filename)

func spawn_cell() -> void:
	if get_parent().is_a_parent_of(self):
		#We need to create an instance of a the current cell type and attach it to the parent level 
		print("cell_type_cursor: " + cell_type_cursor)
		var new_cell
		match cell_type_cursor:
			
			"Organella":
				new_cell = Utils.organella_cell_type.instance()
				
			"Plasmoids":
				new_cell = Utils.plasmoids_cell_type.instance()
			
			_:
				new_cell = Utils.organella_cell_type.instance()
		
		get_parent().add_child(new_cell) #Add to level node
		new_cell.position = self.position


func _on_ClickTimer_timeout():
	click_timer_active = false
