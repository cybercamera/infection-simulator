extends Node2D

signal injecting_vaccine

export(String, "Centrioles", "Plasmoids", "Cilialla", "Vacuella") var vaccination_type  = "Centrioles"

export var vaccination_load: float = 100.0 

const VACCINATION_SHOT = 10
var max_vaccination_load = 100.0

var click_timer_active: bool = false
var mouse_cursor_active: bool = false
#var vaccination_load: int = 10
var in_cell : bool = false


func _ready():
	add_to_group("syringe_cursor")
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	mouse_cursor_active = true
	Utils.syringe_active = false
	$TextureProgress.value = 100
	#Hook up to the HUD to get signals of cursor moving over panel
	var HUD = get_parent().get_node("HUD")
	#print("HUD node: " + str(HUD))
	if HUD:
			HUD.connect("hide_current_cell_cursor", self, "hide_mouse_cursor")
			#We don't need a show_mouse_cursor here, as the cursor is on instantly
			#HUD.connect("show_current_cell_cursor", self, "show_mouse_cursor") 

func _process(delta):
	self.position = self.get_global_mouse_position()
	get_input()
	
func clear_mouse_cursor() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Utils.syringe_active = false
	queue_free()
		
func get_input():
	if !click_timer_active and (Input.is_action_pressed("ui_mouse_left_click") or Input.is_action_pressed("ui_accept")):
		#print("active mouse click")
		click_timer_active = true
		$ClickTimer.start()
		inject_vaccination()
	if Input.is_action_pressed("ui_cancel"):
		clear_mouse_cursor()
		

func inject_vaccination() -> void:
	#We now inject this type of vaccination into the selected cell, if it is a cell
	#print("inject_vaccination() signal")
	#But first. do we vave any vaccine left? And are we inside a cell?
	if vaccination_load >= VACCINATION_SHOT and in_cell:
		#Yes, we have enough for one shot
		emit_signal("injecting_vaccine", vaccination_type, VACCINATION_SHOT)
		vaccination_load -= VACCINATION_SHOT
		$TextureProgress.value = int(vaccination_load/max_vaccination_load * 100) #compute percenta
		print(str(vaccination_load/max_vaccination_load * 100))
		$InjectionAnimation.play("Injection")


func hide_mouse_cursor() -> void:
	#We should only be able to clear the cell cursor when the cursor has moved over the panel for the first time
	#But only after it has left the panel for the first time
	#print("hide_mouse_cursor()")
	if mouse_cursor_active:
		clear_mouse_cursor()
		mouse_cursor_active = false

func show_mouse_cursor() -> void:
	#We should only be able to clear the cell cursor when the cursor has moved over the panel for the first time
	#print("show_mouse_cursor()")
	mouse_cursor_active = true
	
	
func init():
	#This is called after the scene is instanced 
	if get_parent().is_a_parent_of(self):
		var cursor_filename = "res://assets/sprites/Syringe" + vaccination_type + ".png"
		print(cursor_filename)
		$Sprite.texture = load(cursor_filename)



func _on_ClickTimer_timeout():
	click_timer_active = false


func _on_InjectionArea_body_entered(body):
	#Is this entering a cell?
	if body.get_instance_id() != self.get_instance_id():
		#print("contact_area_entered")
		
		if body.get_groups().has("cells"):
			in_cell = true


func _on_InjectionArea_body_exited(body):
	#Is this entering a cell?
	if body.get_instance_id() != self.get_instance_id():
		#print("contact_area_entered")
		
		if body.get_groups().has("cells"):
			in_cell = false
